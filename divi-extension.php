<?php
/*
Plugin Name: Divi Custom Extensions
Plugin URI:  
Description: Divi extension with some custom modules
Version:     1.0.1
Author:      Greg Lorenzen
Author URI:  
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: myex-my-extension
Domain Path: /languages

Parallax Scroll is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Parallax Scroll is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Parallax Scroll. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/


if ( ! function_exists( 'myex_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function myex_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/MyExtension.php';
	wp_enqueue_script('extension-builder-js', plugin_dir_path(__FILE__) . 'scripts/builder-bundle.min.js');
	wp_enqeue_style('extension-style-css', plugin_dir_path(__FILE__) . 'styles/style.min.css');
}
add_action( 'divi_extensions_init', 'myex_initialize_extension' );
endif;
